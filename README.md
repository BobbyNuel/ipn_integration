# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This .net core application provides a detailed description how to integrate the PayPal instant payment notification. 
PayPal Instant Payment Notification (IPN) is a callback system used to notify the client of a completed transaction through email.
To achieve this, IPN variables relating to the transaction is sent to the specified public URL in the request which is verified using 
PayPal servers and getting a response string with either "VERIFIED" or "INVALID�.

### How do I get set up? ###
Install these packages for the .net core project using package manager:

 Mimekit
 Swagger 
 Swagger UI
 Swagger Gen
 RestSharp
 
To configure the ngrok to obtain a public Url for testing that routes to your local host use the following steps:

 Download the ngrok ZIP file.
 Unzip the ngrok.exe file.
 Place the ngrok.exe in /windows/system32 path.
 Make sure the folder is in your PATH environment variable.
 Open bash profile and run the following command ngrok http [port number] -host-header="localhost:[port number]".


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
 You need to have a paypal developer and mailgun accounts to be able to obtain api keys required to successfully carry out this integration.
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin