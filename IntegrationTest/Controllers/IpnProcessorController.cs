﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using IntegrationTest.EmailHandler.Interface;
using IntegrationTest.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace IntegrationTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class IpnProcessorController : ControllerBase
    {
        private IEmailService _emailSerivce;

        public IpnProcessorController(IEmailService emailService)
        {
            _emailSerivce = emailService;
        }

        [HttpPost("Receieve")]
        public IActionResult Receive()
        {
            IpnDto ipnContext = new IpnDto()
            {
                IPNRequest = Request
            };

            using (StreamReader reader = new StreamReader(ipnContext.IPNRequest.Body, Encoding.UTF8))
            {
                ipnContext.RequestBody = reader.ReadToEnd();
            }

            //Store the IPN received from PayPal
            LogRequest(ipnContext);

            //Fire and forget verification task
            Task.Run(() => VerifyTaskAsync(ipnContext));

            //Reply back a 200 code
            return Ok();
        }

        private void VerifyTaskAsync(IpnDto ipnContext)
        {
            try
            {
                var verificationRequest = WebRequest.Create("https://www.sandbox.paypal.com/cgi-bin/webscr");

                //Set values for the verification request
                verificationRequest.Method = "POST";
                verificationRequest.ContentType = "application/x-www-form-urlencoded";

                //Add cmd=_notify-validate to the payload
                string strRequest = "cmd=_notify-validate&" + ipnContext.RequestBody;
                verificationRequest.ContentLength = strRequest.Length;

                //Attach payload to the verification request
                using (StreamWriter writer = new StreamWriter(verificationRequest.GetRequestStream(), Encoding.ASCII))
                {
                    writer.Write(strRequest);
                }

                //Send the request to PayPal and get the response
                using (StreamReader reader = new StreamReader(verificationRequest.GetResponse().GetResponseStream()))
                {
                    ipnContext.Verification = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                //Capture exception for manual investigation
                Log.Error(ex, "Manual investigation exception.");
            }

            ProcessVerificationResponse(ipnContext);
        }


        private void LogRequest(IpnDto ipnContext)
        {
            // Persist the request values into a database or temporary data store
        }

        private void ProcessVerificationResponse(IpnDto ipnContext)
        {
            try
            {
                
                var requestResponseBody = ProcessIpnRequestBody(ipnContext);

                //sends the email notification to the client
                _emailSerivce.SendInstantPaymentNotificationAsync(requestResponseBody, null);

                if (ipnContext.Verification.Equals("VERIFIED"))
                {
                    // check that Payment_status=Completed
                    // check that Txn_id has not been previously processed
                    // check that Receiver_email is your Primary PayPal email
                    // check that Payment_amount/Payment_currency are correct
                    // process payment

                }
                else if (ipnContext.Verification.Equals("INVALID"))
                {
                    //Log for manual investigation
                }
                else
                {
                    //Log error
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Manual investigation exception.");
                
            }
        }

        /// <summary>
        /// This method is responsible for extracting the plain text associated with the request body.
        /// </summary>
        /// <param name="ipnContext">IpnDto which is the data transfer object in the model folder</param>
        /// <returns>IpnResponse</returns>
        /// 
        private IpnResponse ProcessIpnRequestBody(IpnDto ipnContext)
        {
            var response = new IpnResponse();
           var hsParams = new Hashtable();
            hsParams.Clear();

            String[] RSplit = ipnContext.RequestBody.Replace("%20&", "%20and").Replace("%20", " ").Split('&');
            String[] Rsplitx;
            foreach (string s in RSplit)
            {
                Rsplitx = s.Split('=');
                hsParams.Add(Rsplitx[0], Rsplitx[1]);
            }

            response.first_name = hsParams["first_name"].ToString().Trim();
            response.last_name = hsParams["last_name"].ToString().Trim();
            response.payment_date = hsParams["payment_date"].ToString().Replace(" ", "-").Replace("%2C", "").Replace("%", ":").Replace("3A", " ").Trim();
            response.payment_status = hsParams["payment_status"].ToString().Trim();
            response.payment_Type = hsParams["payment_type"].ToString().Trim() ?? null;
            response.payer_status = hsParams["payer_status"].ToString().Trim() ?? null;
            response.payer_email = hsParams["payer_email"].ToString().Trim() ?? null;
            response.payer_id = hsParams["payer_id"].ToString().Trim() ?? null;
            response.address_country = hsParams["address_country"].ToString().Trim() ?? null;
            response.address_status = hsParams["address_status"].ToString().Trim() ?? null;
            response.address_name = hsParams["address_name"].ToString().Trim() ?? null;
            response.address_country_code = hsParams["address_country_code"].ToString().Trim() ?? null;
            response.address_zip = hsParams["address_zip"].ToString().Trim() ?? null;
            response.address_state = hsParams["address_state"].ToString().Trim() ?? null;
            response.address_street = hsParams["address_street"].ToString().Trim() ?? null;
            response.item_name = hsParams["item_name"].ToString().Trim() ?? null;
            response.item_number = hsParams["item_number"].ToString().Trim() ?? null;
            response.quantity = hsParams["quantity"].ToString().Trim() ?? null;
            response.shipping = hsParams["shipping"].ToString().Trim() ?? null;
            response.tax = hsParams["tax"].ToString().Trim() ?? null;
            response.txn_type = hsParams["txn_type"].ToString().Trim() ?? null;
            response.txn_id = hsParams["txn_id"].ToString().Trim() ?? null;
            response.notify_version = hsParams["notify_version"].ToString().Trim() ?? null;
            response.invoice = hsParams["invoice"].ToString().Trim() ?? null;
            response.receiver_email = hsParams["receiver_email"].ToString().Trim() ?? null;
            response.verify_sign = hsParams["verify_sign"].ToString().Trim() ?? null;
            response.residence_country = hsParams["residence_country"].ToString().Trim() ?? null;
            response.mc_gross = hsParams["mc_gross"].ToString().Trim() ?? null;
            response.mc_currency = hsParams["mc_currency"].ToString().Trim() ?? null;
            response.verification = ipnContext.Verification;


            hsParams.Clear();

            return response;

           
        }

    }
}