﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTest.Utility
{
    public class EmailRecipient
    {
        public List<string> ManagementEmails { get; set; }
    }
}
