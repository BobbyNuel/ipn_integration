﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTest.Utility
{
    public class EmailConfiguration
    {
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string LocalDomain { get; set; }
        public string ProductionDomain { get; set; }
        public string VerificationEndPoint { get; set; }
        public string ResetPasswordEndPoint { get; set; }
        public string SendGridKey { get; set; }
    }
}
