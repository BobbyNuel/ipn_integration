﻿using IntegrationTest.EmailHandler.Interface;
using IntegrationTest.Model;
using IntegrationTest.Utility;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using RestSharp;
using RestSharp.Authenticators;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace IntegrationTest.EmailHandler.Implementation
{
    public class EmailClient : IEmailClient
    {
        private readonly EmailConfiguration ec;

        public EmailClient(IOptions<EmailConfiguration> emailConfig)
        {
            ec = emailConfig.Value;
        }

        private async Task PushMailAsync(List<string> recipients, string subject, string message, List<EmailAttachments> attachments, List<string> CC, List<string> BCC)
        {
            try
            {
                
                var to = recipients.Select(x => MailHelper.StringToEmailAddress(x)).ToList().FirstOrDefault();
                var from = new EmailAddress(ec.FromAddress, ec.FromName);
                var attachment = attachments.Select(x => x.Filename).FirstOrDefault();

                RestClient client = new RestClient();
                client.BaseUrl = new Uri("https://api.mailgun.net/v3");
                client.Authenticator =
                    new HttpBasicAuthenticator("api",
                        "key-06b6db8ba1fa36f7dfddcff9d7c54040");
                RestRequest request = new RestRequest();
                request.AddParameter("domain", "audme.dreamteam.com.ng", ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from.Email);
                request.AddParameter("to", to.Email);
                request.AddParameter("subject", subject);
                request.AddParameter("html", message);
                request.AddParameter("cc", BCC.FirstOrDefault());
                request.AddFile("attachment", attachment) ;
                request.Method = Method.POST;

                var response = await client.ExecuteAsync(request);
            }
            catch (Exception)
            {
                return;
            }
        }
        
        public async Task SendEmailAsync(List<string> recipients, string subject, string message, List<EmailAttachments> attachments, List<string> CC, List<string> BCC)
        {
            await Task.Factory.StartNew(async () => {
                await PushMailAsync(recipients, subject, message, attachments, CC, BCC);
            });
        }
    }
}
