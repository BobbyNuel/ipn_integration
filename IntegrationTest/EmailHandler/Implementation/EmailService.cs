﻿using IntegrationTest.EmailHandler.Interface;
using IntegrationTest.Model;
using IntegrationTest.Utility;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationTest.EmailHandler.Implementation
{
    public class EmailService : IEmailService
    {
       
        private readonly IEmailClient _client;
        private readonly IHostEnvironment _env;
        private readonly EmailRecipient _emailRecipient;

        public EmailService(IEmailClient client, IHostEnvironment env, IOptions<EmailRecipient> emailRecipient)
        {
            _client = client;
            _env = env;
            _emailRecipient = emailRecipient.Value;
        }


        private async Task<string> GetTemplate(string TemplateFileName)
        {
            var pathToFile = _env.ContentRootPath
                           + Path.DirectorySeparatorChar.ToString() + "Template"
                           + Path.DirectorySeparatorChar.ToString() + TemplateFileName;

            var builder = new BodyBuilder();
            using (StreamReader SourceReader = File.OpenText(pathToFile))
            {
                builder.HtmlBody = await SourceReader.ReadToEndAsync();
            }

            return builder.HtmlBody;
        }

        public async Task<bool> SendInstantPaymentNotificationAsync(IpnResponse response, string helpUrl)
        {

            var pathToFile = _env.ContentRootPath
                           + Path.DirectorySeparatorChar.ToString() + "Images"
                           + Path.DirectorySeparatorChar.ToString() + "1280px-PayPal.svg";

            byte[] imageArray = File.ReadAllBytes(pathToFile);

            var attachment = new EmailAttachments() { Filename = pathToFile,Bytes = imageArray};

            bool isSuccessful = true;
            string template = await GetTemplate("PaymentNotification.html");
            string mailBody = template.Replace("{{invoice}}", response.invoice)
                                      .Replace("{{item_name}}", response.item_name)
                                      .Replace("{{item_number}}", response.item_number)
                                      .Replace("{{txn_type}}", response.txn_type)
                                      .Replace("{{txn_id}}", response.txn_id)
                                      .Replace("{{payment_Type}}", response.payment_Type)
                                      .Replace("{{payment_date}}", response.payment_date)
                                      .Replace("{{payer_id}}", response.payer_id)
                                      .Replace("{{mc_gross}}", response.mc_gross)
                                      .Replace("{{support_email}}", "mailto:paypal@support.com")
                                      .Replace("{{mc_gross}}", response.mc_gross)
                                      .Replace("{{mc_currency}}", response.mc_currency)
                                      .Replace("{{payment_status}}", response.payment_status)
                                      .Replace("{{verification}}", response.verification)
                                      .Replace("{{help_url}}", helpUrl);
            
            string subject = $"IPN for Invoice-{response.invoice}";
            List<string> BCC = _emailRecipient.ManagementEmails;
            await _client.SendEmailAsync(new List<string> { response.receiver_email }, subject, mailBody, new List<EmailAttachments> { attachment }, null, BCC);

            return isSuccessful;
        }

    }
}
