﻿using IntegrationTest.Model;
using IntegrationTest.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTest.EmailHandler.Interface
{
    public interface IEmailService
    {
        Task<bool> SendInstantPaymentNotificationAsync(IpnResponse response, string helpUrl);
    }
}
