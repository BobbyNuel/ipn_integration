﻿using IntegrationTest.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTest.EmailHandler.Interface
{
    public interface IEmailClient
    {
        Task SendEmailAsync(List<string> recipient, string subject, string message, List<EmailAttachments> attachments, List<string> CC, List<string> BCC);
    }
}
